package br.com.ecosystem.domain.model;

import javax.validation.constraints.NotEmpty;

public class UserRequestDto {
	
	@NotEmpty
	private String name;
	@NotEmpty
	private String email;
	@NotEmpty
	private String login;
	@NotEmpty
	private String password;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	

}
